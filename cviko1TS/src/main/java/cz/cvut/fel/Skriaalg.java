package cz.cvut.fel;

public class Skriaalg {
    public long factorialIterative(int n) {
        int result = 1;
        int i = 1;
        while (i <= n) {
            result *= i;
            i++;
        }
        return result;
    }

    public long factorialRecursive(int g) {
        if (g <= 1) {
            return 1;
        } else {
            return g * factorialRecursive(g - 1);
        }
    }

}
