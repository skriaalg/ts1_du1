package cz.fel.cvut.ts1;

import cz.cvut.fel.Skriaalg;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SkriaalgTest {
    @Test
    public void factorialIterativeTest() {
        Skriaalg skriaalg = new Skriaalg();
        int n = 2;
        long expectedRes = 2;
        long res = skriaalg.factorialIterative(n);

        Assertions.assertEquals(expectedRes, res);
    }
    @Test
    public void factorialRecursiveTestStudent2() {
        Skriaalg skriaalg = new Skriaalg();
        int n = 3;
        long expectedRes = 6;
        long res = skriaalg.factorialRecursive(n);

        Assertions.assertEquals(expectedRes, res);
    }
}
